//
//  Result.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 12/25/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import Foundation

enum Result<T> {
    case Success(T)
    case Failure
}
