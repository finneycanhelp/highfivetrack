//
//  NotesEntryViewModel.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 11/23/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import Foundation
import RxSwift

class NotesEntryViewModel {
    
    var selectedPersonVariable:Variable<Person>
    
    let bag = DisposeBag()

    // besides a "mirror"  There may be a way to "wrap" the last note in the person's notes array.
    var currentNoteContentMirror:Variable<String> = Variable("")

    init(initialPersonVariable:Variable<Person>) {
        selectedPersonVariable = initialPersonVariable
    }
    
    var currentNoteContent:Observable<String> {
        let selectedPersonNoteContent = selectedPersonVariable
            .asObservable().map { person in person.notes.last!.text }
        
        return Observable.of(currentNoteContentMirror.asObservable(), selectedPersonNoteContent)
        .merge()
    }

    func setCurrentNoteContent(content: String) {
        PersonDatabaseManager.shared.setTextOnNote(content, note:currentNote())
        currentNoteContentMirror.value = content
    }
    
    private func currentNote() -> Note {
        
        let someValue = selectedPersonVariable.value.notes.last
        if someValue == nil {
            print("something wrong 2")
            return Note()
        }
        
        return someValue!
    }
    
    func currentNoteContentIsEmpty() -> Observable<Bool> {
        
        return currentNoteContent.map{ (string) -> Bool in
            return string.isEmpty
        }
        
    }
    
    func clearCurrentNoteContent() {
        
        setCurrentNoteContent("")
    }
    
    func saveAndAddNewCurrentNote() {
        
        PersonDatabaseManager.shared.saveNote(currentNote())
        
        addNewCurrentNote()
    }

    func status() {
        PersonDatabaseManager.shared.status()
    }

    private func addNewCurrentNote() {
        
        let person = selectedPersonVariable.value
        
        PersonDatabaseManager.shared.createPersistedNoteForPerson(person)
        
    }
}
