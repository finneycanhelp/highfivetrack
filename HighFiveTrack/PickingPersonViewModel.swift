//
//  PeopleModelView.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 11/23/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import Foundation
import RxSwift

class PickingPersonViewModel {

    func setSelectedPersonAsItem(item: Int) {
        
        PersonManager.shared.setSelectedPersonAsItem(item)
    }

    func count() -> Int {
     
        return PersonManager.shared.count()
    }
    
    func nameForPersonAtIndex(index: Int) -> String {
        return PersonManager.shared.personAtIndex(index).name
    }

    func selectedPersonVariable() -> Variable<Person> {
        return PersonManager.shared.selectedPersonVariable()
    }
}
