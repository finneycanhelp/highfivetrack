//
//  NotesTableViewController.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 12/28/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift
import RxCocoa

class NotesListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let disposeBag = DisposeBag()
    
    var notesViewModel: NotesViewModel!

    override func viewDidLoad() {

        super.viewDidLoad()
        
        notesViewModel = NotesViewModel(personSelected: PersonManager.shared.selectedPersonVariable())
        
        notesViewModel.notesList().bindTo(tableView.rx_itemsWithCellIdentifier("noteCell")) { (row, note, cell) in
                cell.textLabel?.text = "\(note.text) @ row \(row)"
            }
            .addDisposableTo(disposeBag)
    }
}
