//
//  PersonManager.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 12/28/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import Foundation
import RxSwift

class PersonManager {
    
    static let noteAddedNotificationString = "note_added"
    
    var pickerData:[Person]

    private let selectedPerson: Variable<Person>

    static let shared = PersonManager()

    init() {
        
        pickerData = Array(PersonDatabaseManager.shared.loadPersonObjects())
        
        if pickerData.count == 0 {
            
            for i in 1...6 {
                
                let person = PersonDatabaseManager.shared.createPersistedPersonWithNewNote("Item \(i)")
                pickerData.append(person)
            }
        }
        
        selectedPerson = Variable<Person>(pickerData.first!)
    }

    func setSelectedPersonAsItem(item: Int) {
        
        guard item >= 0 && item < pickerData.count else {
            print("item out of bounds")
            return
        }
        
        selectedPerson.value = pickerData[item]
    }

    func count() -> Int {
        
        return pickerData.count
    }
    
    func nameForPersonAtIndex(index: Int) -> String {
        return personAtIndex(index).name
    }
    
    func selectedPersonVariable() -> Variable<Person> {
        return selectedPerson
    }
    
    func personAtIndex (index: Int) -> Person {
        return pickerData[index];
    }

    
}