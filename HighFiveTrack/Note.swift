//
//  Note.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 11/23/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import Foundation
import RealmSwift

class Note: Object {
 
    dynamic var text: String = ""
    
}