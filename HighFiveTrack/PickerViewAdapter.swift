//
//  PickerViewAdapter.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 11/23/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import UIKit
import RxSwift

class PickerViewAdapter: NSObject {
    
    let pickingPersonViewModel: PickingPersonViewModel
    
    init(pickingPersonViewModel: PickingPersonViewModel) {
        
        self.pickingPersonViewModel = pickingPersonViewModel
    }
    
    func selectedPersonVariable() -> Variable<Person> {
        return self.pickingPersonViewModel.selectedPersonVariable()
    }
    
}

extension PickerViewAdapter: UIPickerViewDelegate {

    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.pickingPersonViewModel.setSelectedPersonAsItem(row)
    }

    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickingPersonViewModel.nameForPersonAtIndex(row)
    }
}

extension PickerViewAdapter: UIPickerViewDataSource {

    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickingPersonViewModel.count()
    }

}
