//
//  Person.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 12/23/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import Foundation
import RealmSwift

class Person: Object, Hashable, Equatable {
    
    dynamic var name = ""
    dynamic var uniqueId = NSUUID().UUIDString
    
    let notes = List<Note>()
    
    convenience init(name: String) {
        self.init()
        self.name = name
    }
    
    override var hashValue: Int { get {
        return name.hashValue
    }}

    func addNote(note: Note) {
        notes.append(note)
    }
    
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }

    override static func primaryKey() -> String? {
        return "uniqueId"
    }
    
}

func == (lhs: Person, rhs: Person) -> Bool {
    return lhs.uniqueId == rhs.uniqueId
}
