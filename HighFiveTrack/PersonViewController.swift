//
//  AddItemViewController.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 11/23/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PersonViewController: UIViewController {

    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    
    var selectedPersonVariable: Variable<Person> {
        
        return pickerViewAdapter.selectedPersonVariable()
    }
    
    let pickerViewAdapter: PickerViewAdapter = PickerViewAdapter(pickingPersonViewModel: PickingPersonViewModel())
    var notesEntryViewModel: NotesEntryViewModel!
    
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        notesEntryViewModel = NotesEntryViewModel(initialPersonVariable: selectedPersonVariable)
        
        registerItems()
    }
    
    @IBAction func statusWasTapped(sender: AnyObject) {
        notesEntryViewModel.status()
    }
    
    func registerItems() {
        self.picker.dataSource = pickerViewAdapter
        self.picker.delegate = pickerViewAdapter

        // TODO: change to bindTo and rename currentNoteContentVariable
        self.notesEntryViewModel.currentNoteContent
        .subscribeNext { [weak self] content in
        
            guard let strongSelf = self else { return }
        
            strongSelf.notesTextView.text = content
        }.addDisposableTo(bag)
        
        doneButton.rx_tap.subscribeNext { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.notesTextView.endEditing(true)
            
        }.addDisposableTo(bag)
        
        clearButton.rx_tap.subscribeNext { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.notesEntryViewModel.clearCurrentNoteContent()
            
            self?.notesTextView.text = ""
            
        }.addDisposableTo(bag)
        
        self.notesEntryViewModel.currentNoteContentIsEmpty()
            .map { !$0 }
            .bindTo(self.addButton.rx_enabled)
            .addDisposableTo(bag)
        
        self.notesTextView.rx_text
            .map { [unowned self] text in
                return !self.notesTextViewIsEmpty()
            }
            .bindTo(self.addButton.rx_enabled)
            .addDisposableTo(bag)
        
        self.notesTextView.rx_text.subscribeNext { [weak self] content in
            
            guard let strongSelf = self else { return }

            strongSelf.notesEntryViewModel.setCurrentNoteContent(content)
        }
        .addDisposableTo(bag)
        
        addButton.rx_tap.subscribeNext { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.notesEntryViewModel.saveAndAddNewCurrentNote()
            
            strongSelf.notesTextView.text = ""
            
        }.addDisposableTo(bag)
    }
    
    func notesTextViewIsEmpty() -> Bool {
        return self.notesTextView.text.isEmpty
    }
    
}
