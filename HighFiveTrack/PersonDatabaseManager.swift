//
//  NoteManager.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 12/23/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

class PersonDatabaseManager {

    let realm: Realm
    static let shared = PersonDatabaseManager()

    init() {
        let tempRealm = try! Realm()
        realm = tempRealm
    }

    func createPersistedPersonWithNewNote(name: String) -> Person {
        
        let person = Person(name: name)
        let note = Note()

        person.addNote(note)
        
        try! self.realm.write {
            self.realm.add(note)
            self.realm.add(person)
        }

        notifyAddedNote(note)

        return person
    }
    
    func createPersistedNoteForPerson(person:Person) -> Note {
        
        let note = Note()
        
        try! realm.write() {
            realm.add(note)
            person.addNote(note)
        }
        
        notifyAddedNote(note)
        
        return note
    }
    
    func clearNote(note: Note) {
        
        try! self.realm.write {
            note.text = ""
        }
    }
        
    func setTextOnNote(text:String, note:Note) {
        try! self.realm.write {
            note.text = text
        }
    }
    
    func saveNote(note: Note) {
        
        try! self.realm.write {
            self.realm.add(note)
        }
    }
    
    func loadPersonObjects() -> Results<Person> {
        
        let personResults:Results<Person> = realm.objects(Person)
        return personResults
    }
    
    func status() {
//        let noteResults = realm.objects(Note)
        let personResults = realm.objects(Person)
  
// FIXME: why is there an extra note in there?
//        print("notes: + \(noteResults)")
        print("personResults: + \(personResults)")
    }
    
    private func notifyAddedNote(note:Note) {
        NSNotificationCenter.defaultCenter().postNotificationName(PersonManager.noteAddedNotificationString, object: note)
    }
}