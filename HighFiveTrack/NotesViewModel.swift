//
//  NotesViewModel.swift
//  HighFiveTrack
//
//  Created by Michael Finney on 12/28/15.
//  Copyright © 2015 Michael Finney. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

class NotesViewModel {

    private let personSelected: Variable<Person>

    let bag = DisposeBag()

    init(personSelected: Variable<Person>) {
        self.personSelected = personSelected
    }

    func notesList() -> Observable<[Note]> {
        
        let notificationToNotes = NSNotificationCenter.defaultCenter().rx_notification(PersonManager.noteAddedNotificationString)
        .map {[weak self] (notification) -> [Note] in
            
            guard let strongSelf = self else { return Array<Note>() }
            
            return strongSelf.pastNotesFromList(strongSelf.personSelected.value.notes)
        }
        
        let selectedPersonToNotes = personSelected.asObservable()
        .map {[unowned self] (person) -> [Note] in

            return self.pastNotesFromList(person.notes)
        }
        
        return Observable.of(selectedPersonToNotes, notificationToNotes).merge()
    }
    
    private func pastNotesFromList(notesList: List<Note>) -> [Note] {
        var pastNotes = Array(notesList)
        pastNotes.removeLast()
        return pastNotes
    }
}